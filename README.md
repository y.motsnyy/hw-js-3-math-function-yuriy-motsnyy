Функции нужны, чтобы не писать каждый раз повторяющийся код. Написав такую часть кода один раз к нему можно вернуться из другого места программы и выполнить код, вызвав соответствующую функцию, в которой этот код прописан.

Функция может содержать несколько параметров записанных в круглых скобках через запятую. В эти параметры можно передать значения аргументов, которые функция будет использовать для вычислений в своем теле окруженном фигурными скобками. 
