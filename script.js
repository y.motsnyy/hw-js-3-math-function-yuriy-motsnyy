let firstNum;
let secondNum;
let sign; 

do {
  firstNum = +prompt('Enter your first number');
} while(Number.isNaN(firstNum));

do {
  secondNum = +prompt('Enter your second number');
} while(Number.isNaN(secondNum));

do {
  sign = prompt("Enter one of sings +, -, *, /", "+,  -,  *,  /");
} while(sign !== '+' && sign !== '-' && sign !== '*' && sign !== '/');

function calc(a, b, op) {
  let result;          
    switch (op) {
      case '+':
        result = a + b;
        break;
      case '-':
        result = a - b;
        break;
      case '*':
        result = a * b;
        break;
      case '/':
        result = a / b;
        break;
  }
  return result;
}
console.log(calc(firstNum, secondNum, sign));
